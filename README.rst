Jibrel network test issue
============================================

Project started without any symbols in database and
the symbols should be added by api.
After that 100 latest candles, except the latest one, will be added
to the database and the project is ready to use. It means what the
latest (today) candle is never presented at the database. The reason
of that behaviour is what that candle is not have close price yet.
Each symbol get the latest (yesterday) candle at 00:01 by schedule.

Use the following command to build and start the project

::

    docker-compose build
    docker-compose up &

Credentials described at ./docker/dev.env

::

    source <(sed -E -n 's/^BASIC+/export &=&/ p' ./docker/dev.env)

Usage
============================================

Add new pair. CSRF is disabled in debug mode for the easy console usage.

::

    curl -L -X POST http://localhost:8000/currencies/ -H "Content-Type: application/json" -u $BASIC_USERNAME:$BASIC_PASSWORD --data "{\"name\": \"BTCUSD\"}"


Get list of the all available currencies

::

    curl -L http://localhost:8000/currencies/ -u $BASIC_USERNAME:$BASIC_PASSWORD

Get latest (yesterday) rate and 10d avg volume

::

    curl -L http://localhost:8000/rate/BTCUSD/ -u $BASIC_USERNAME:$BASIC_PASSWORD




Testing
============================================

::

    docker exec -i jbti_backend_1 python manage.py test
