import os
import json
import decimal
from operator import attrgetter
from base64 import b64encode
from datetime import timedelta
from django.test import TestCase
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.reverse import reverse as api_reverse
from rest_framework.test import APIClient
from .tasks import get_rates
from .models import Symbol, Candle
from .signals.handler import symbol_created

User = get_user_model()


class JBTITestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user = User(
            username=os.environ.get('BASIC_USERNAME')
        )
        user.set_password(os.environ.get('BASIC_PASSWORD'))
        user.save()
        cls.yesterday = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(1)
        cls.yts = cls.yesterday.timestamp() * 1000
        post_save.disconnect(symbol_created, sender=Symbol)

    def setUp(self):
        super().setUp()
        self.__api_client__ = APIClient()
        credentials = b64encode('{username}:{password}'.format(
            username=os.environ.get('BASIC_USERNAME'),
            password=os.environ.get('BASIC_PASSWORD')
        ).encode()).decode("ascii")
        self.__api_client__.credentials(
            HTTP_AUTHORIZATION='Basic {}'.format(credentials)
        )

    def test_auth(self):
        client = APIClient()
        credentials = b64encode('{username}:{password}'.format(
            username=os.environ.get('BASIC_USERNAME'),
            password=os.environ.get('BASIC_PASSWORD') + 'WRONG'
        ).encode()).decode("ascii")
        client.credentials(
            HTTP_AUTHORIZATION='Basic {}'.format(credentials)
        )
        url = api_reverse('symbol-list')
        response = client.get(url, format='json')
        assert response.status_code == 401

    def test_auth_no_creditnails(self):
        client = APIClient()
        url = api_reverse('symbol-list')
        response = client.get(url, format='json')
        assert response.status_code == 401

    def test_symbol_create(self, *args, **kwargs):
        symbol_name = 'BTCUSD'
        url = api_reverse('symbol-list')
        response = self.__api_client__.post(url, data={
            'name': symbol_name
        }, format='json')
        content = json.loads(response.content.decode('utf-8'))
        assert response.status_code == 201
        assert content.get('name') == symbol_name

    def test_symbol_create_not_exist(self, *args, **kwargs):
        symbol_name = 'BTCUSD1'
        url = api_reverse('symbol-list')
        response = self.__api_client__.post(url, data={
            'name': symbol_name
        }, format='json')
        content = json.loads(response.content.decode('utf-8'))
        assert response.status_code == 400
        assert content.get('name') is not None

    def test_symbol_create_alrady_exist(self, *args, **kwargs):
        symbol_name = 'BTCUSD'
        Symbol.objects.create(name=symbol_name)
        url = api_reverse('symbol-list')
        response = self.__api_client__.post(url, data={
            'name': symbol_name
        }, format='json')
        content = json.loads(response.content.decode('utf-8'))
        assert response.status_code == 400
        assert content.get('name') is not None

    def test_symbol_update(self):
        symbol_name = 'BTCUSD'
        Symbol.objects.create(name=symbol_name)
        get_rates()
        qs = Candle.objects.all()
        assert qs.count() == 1
        assert qs[0].pair.name == symbol_name
        assert qs[0].timestamp == self.yts

    def test_symbol_list(self):
        symbol_name = 'BTCUSD'
        Symbol.objects.create(name=symbol_name)
        url = api_reverse('symbol-list')
        response = self.__api_client__.get(url, format='json')
        content = json.loads(response.content.decode('utf-8'))
        assert response.status_code == 200
        assert len(content) == 1
        assert content[0].get('name') == symbol_name

    def test_symbol_rate(self):
        symbol_name = 'BTCUSD'
        Symbol.objects.create(name=symbol_name)
        get_rates([symbol_name])
        url = api_reverse('symbol-detail', kwargs={'pk': symbol_name})
        response = self.__api_client__.get(url, format='json')
        content = json.loads(response.content.decode('utf-8'))
        qs = Candle.objects.filter(pair__name=symbol_name)
        avg = sum(map(attrgetter('volume'), qs[:10])) / 10

        qntz = lambda value, field: decimal.Decimal(value).quantize(decimal.Decimal('1E-{}'.format(Candle._meta.get_field(field).decimal_places)))

        assert response.status_code == 200
        assert qntz(content['volume_avg'], 'volume') == qntz(avg, 'volume')
        assert content['timestamp'] == self.yts
        assert qntz(content['close'], 'close') == qs[0].close
