from django.apps import AppConfig


class JBTIConfig(AppConfig):
    name = 'jbti'

    def ready(self):
        """
        Signals connection

        :return:
        """
        import jbti.signals.handler
