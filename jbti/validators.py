from django.core.validators import deconstructible, ValidationError
from django.utils.translation import gettext_lazy as _
import urllib.request
import json


@deconstructible
class SymbolExistValidator:
    message = _('Symbol does not exist at Bitfinex')
    code = 'bad_symbol'

    def __init__(self, message=None):
        if message:
            self.message = message

    def __call__(self, value):
        """
        Validate that the input contains (or does *not* contain, if
        inverse_match is True) a match for the regular expression.
        """
        endpoint = 'https://api.bitfinex.com/v1/symbols'
        r = urllib.request.urlopen(endpoint)
        if not (r.getcode() == 200 and value.lower() in json.loads(r.read())):
            raise ValidationError(self.message, code=self.code)

