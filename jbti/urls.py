"""jbti URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.views.generic.base import RedirectView
from rest_framework.routers import SimpleRouter
from .api import SymbolViewSet


class OptionalSlashRouter(SimpleRouter):
    def __init__(self):
        self.trailing_slash = '/?'
        super(SimpleRouter, self).__init__()


router = OptionalSlashRouter()
router.register('currencies', SymbolViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('rate/<slug:pk>/', SymbolViewSet.as_view({'get': 'retrieve'})),
    #path('rate/', RedirectView.as_view(url='/symbol/rate/', permanent=True), name='rate')

]
