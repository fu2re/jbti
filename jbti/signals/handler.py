from django.db.models.signals import post_save
from django.dispatch import receiver
from ..models import Symbol
from ..tasks import get_rates


@receiver(post_save, sender=Symbol)
def symbol_created(sender, instance, created, **kwargs):
    """
    get all available rates immediately as soon as new pair added

    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    if not created:
        return
    get_rates.delay([instance.name])
