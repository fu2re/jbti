from rest_framework.response import Response
from rest_framework import serializers, viewsets
from datetime import timedelta
from django.db.models import Avg
from django.utils import timezone
from .models import Symbol, Candle


class SymbolSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Symbol
        fields = ('name',)


class SymbolViewSet(viewsets.ModelViewSet):
    queryset = Symbol.objects.all()
    serializer_class = SymbolSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        Outputs yesterday closed price,
        and 10-days avg volume

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        name = kwargs.get('pk')
        try:
            d = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(1)
            ts = d.timestamp()
            candles = Candle.objects.filter(
                pair__name__iexact=name,
                timestamp__gt=(ts - 10 * 86400) * 1000
            )
            volume_avg = candles.aggregate(volume_avg=Avg('volume'))['volume_avg']
            resp = {
                'close': candles[0].close,
                'volume_avg': volume_avg,
                'timestamp': candles[0].timestamp
            }
        except IndexError:
            resp = {}
        return Response(resp)


