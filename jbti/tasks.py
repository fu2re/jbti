import itertools
import asyncio
from operator import itemgetter
from django.conf import settings
from django.utils import timezone
from celery import task
from bfxapi import Client
from .models import Symbol, Candle

MAPPER = {
    '1m': 1,
    '5m': 5,
    '1h': 60,
    '4h': 240,
    '1d': 1440,
    '1w': 1440 * 7
}
LIMIT = 100


async def __get_rate__(bfx, symbol, end, period, initial):
    """
    get candles for the symbol

    :param bfx: client
    :param symbol: symbol name e.g. ETHUSD
    :param end: timestamp (ms)
    :param period: timeframe
    :param initial: is initial
    :return:
    """
    limit = LIMIT if initial else 1
    start = int(end - 60 * MAPPER[period] * limit * 1000)
    candles = await bfx.rest.get_public_candles(
        't{}'.format(symbol.name.upper()),
        start=start,
        end=int(end),
        section='hist',
        tf=period.upper(),
        limit=limit
    )

    existed = Candle.objects.filter(
        pair=symbol,
        timestamp__in=list(itemgetter(0)(candles))
    ).values_list('timestamp', flat=True)

    Candle.objects.bulk_create(
        Candle(
            pair=symbol,
            timestamp=mts,
            close=close,
            volume=volume
        ) for mts, open, close, high, low, volume in candles
        if mts not in existed
    )


async def __get_rates__(symbol_names=None):
    """
    Get latest rates for the all available symbols.
    If symbols is provided as an argument it tries to get 100 latest data instead.
    ONLY completed candles is saved

    :param symbols: Symbol instances.
    :return:
    """
    bfx = Client()
    initial = bool(symbol_names)
    symbols = Symbol.objects.filter(name__in=symbol_names) if symbol_names else Symbol.objects.all()

    end = (timezone.now().replace(hour=0, minute=0, second=0, microsecond=0).timestamp() - 86400) * 1000
    tasks = [__get_rate__(bfx, symbol, end, period, initial)
             for period, symbol in itertools.product(settings.JBTI_TIME_FRAMES, symbols)]
    await asyncio.gather(*tasks)


@task
def get_rates(symbol_names=None):
    """
    Get latest rates for the all available symbols.
    If symbols is provided as an argument it tries to get 100 latest candles instead.
    :param symbols: Symbol instances.
    :return:
    """
    asyncio.run(__get_rates__(symbol_names))
