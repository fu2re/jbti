from django.db import models
from .validators import SymbolExistValidator


class Symbol(models.Model):
    name = models.CharField(
        validators=[SymbolExistValidator()],
        max_length=16, primary_key=True
    )


class Candle(models.Model):
    pair = models.ForeignKey(Symbol, on_delete=models.CASCADE, related_name='candles')
    timestamp = models.BigIntegerField()
    period = models.CharField(choices=(
        ('1d', '1d'),
    ), default='1d', max_length=2)
    volume = models.DecimalField(max_digits=16, decimal_places=8)
    close = models.DecimalField(max_digits=16, decimal_places=8)

    class Meta:
        ordering = ['-timestamp']
        unique_together = ('timestamp', 'pair', 'period')
