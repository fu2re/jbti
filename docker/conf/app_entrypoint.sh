#!/bin/sh
cd /app
./manage.py migrate --noinput --fake-initial || exit 1
echo "from django.contrib.auth import get_user_model; \
User = get_user_model(); \
User.objects.create_superuser('$BASIC_USERNAME', 'admin@example.com', '$BASIC_PASSWORD') \
if not User.objects.filter(username='$BASIC_USERNAME') else None" | python manage.py shell

celery worker -A jbti -B -c 1 &
./manage.py runserver 0.0.0.0:8000
